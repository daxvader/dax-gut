<?php
/**
 * Enqueue scripts.
 *
 * @package Dax_Gutenberg_Starter
 */

/**
 * Enqueue CSS styles.
 */
function dax_gut_styles() {
	wp_register_style( 'styles', get_template_directory_uri() . '/assets/css/styles.min.css', array(), '1.0.0', 'all' );
	wp_enqueue_style( 'styles' );
}
add_action( 'wp_enqueue_scripts', 'dax_gut_styles' );
add_action( 'login_enqueue_scripts', 'dax_gut_styles', 10 ); // Add styles to login area.

/**
 * Enqueue JS scripts.
 */
function dax_gut_scripts() {

	// Deregister local WP jQuery in frontend and enqueue it from Google libraries.
	if ( is_admin() ) {
		return;
	} else {
		wp_deregister_script( 'jquery' );
	}
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.2.1', true );

	// Enqueue local scripts.
	wp_enqueue_script( 'dax-gut-scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), '1.0.0', 'all' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'dax_gut_scripts' );
