<?php
/**
 * Register sidebar areas.
 *
 * @package Dax_Gutenberg_Starter
 */

if ( ! function_exists( 'dax_gut_sidebars' ) ) {

	/**
	 * Registra las sidebars.
	 */
	function dax_gut_sidebars() {
		$args = array(
			'id'            => 'sidebar_main',
			'class'         => 'sidebar_main',
			'name'          => 'Main sidebar',
			'description'   => 'Main sidebar',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div>',
			'before_widget' => '<div class="widget %2$s">',
			'after_widget'  => '</div>',
		);
		register_sidebar( $args );

		$args = array(
			'id'            => 'sidebar_secondary',
			'class'         => 'sidebar_secondary',
			'name'          => 'Secondary sidebar',
			'description'   => 'Secondary sidebar',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div>',
			'before_widget' => '<div class="widget %2$s">',
			'after_widget'  => '</div>',
		);
		register_sidebar( $args );

	}

	add_action( 'widgets_init', 'dax_gut_sidebars' );

}
