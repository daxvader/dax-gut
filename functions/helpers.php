<?php
/**
 * Funciones de ayuda.
 *
 * @package Dax_Gutenberg_Starter
 */

/**
 * Text to show in admin footer.
 */
function custom_admin_footer() {
	echo 'Developed by <a href="#" target="_blank">John Doe</a>';
}
add_filter( 'admin_footer_text', 'custom_admin_footer' );

/**
 * Returns an array of HTML tags to be printed with the echo wp_kses
 * https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
 *
 * @return array $tags la lista de tags permitidos.
 */
function tags_allowed() {

	$tags = array(
		'a'          => array(
			'class' => array(),
			'href'  => array(),
			'rel'   => array(),
			'title' => array(),
		),
		'abbr'       => array(
			'title' => array(),
		),
		'b'          => array(),
		'blockquote' => array(
			'cite' => array(),
		),
		'cite'       => array(
			'title' => array(),
		),
		'code'       => array(),
		'del'        => array(
			'datetime' => array(),
			'title'    => array(),
		),
		'dd'         => array(),
		'div'        => array(
			'class' => array(),
			'title' => array(),
			'style' => array(),
		),
		'dl'         => array(),
		'dt'         => array(),
		'em'         => array(),
		'h1'         => array(),
		'h2'         => array(),
		'h3'         => array(),
		'h4'         => array(),
		'h5'         => array(),
		'h6'         => array(),
		'i'          => array(
			'class' => array(),
		),
		'img'        => array(
			'alt'    => array(),
			'class'  => array(),
			'height' => array(),
			'src'    => array(),
			'width'  => array(),
		),
		'li'         => array(
			'class' => array(),
		),
		'ol'         => array(
			'class' => array(),
		),
		'p'          => array(
			'class' => array(),
		),
		'q'          => array(
			'cite'  => array(),
			'title' => array(),
		),
		'span'       => array(
			'class' => array(),
			'title' => array(),
			'style' => array(),
		),
		'strike'     => array(),
		'strong'     => array(),
		'ul'         => array(
			'class' => array(),
		),
		'form'       => array(),
		'input'      => array(
			'type'        => array(
				'email'  => array(),
				'submit' => array(),
			),
			'class'       => array(),
			'placeholder' => array(),
			'value'       => array(),
			'style'       => array(),
		),
		'label'      => array(),
		'textarea'   => array(),
		'svg'        => array(),
	);

	return $tags;
}
