<?php
/**
 * Theme support.
 *
 * @package Dax_Gutenberg_Starter
 */

if ( ! function_exists( 'dax_gut_setup' ) ) :

	/**
	 * Add several theme support functions.
	 *
	 */
	function dax_gut_setup() {

		load_theme_textdomain( 'dax-gut', get_template_directory() . '/languages' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add styles.css as editor style https://codex.wordpress.org/Editor_Style.
		add_editor_style( '/css/styles.min.css' );

		add_theme_support( 'menus' );

		add_theme_support( 'custom-background', apply_filters( 'dax_gut_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

	}
endif;
add_action( 'after_setup_theme', 'dax_gut_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dax_gut_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'dax_gut_content_width', 800 );
}
add_action( 'after_setup_theme', 'dax_gut_content_width', 0 );
