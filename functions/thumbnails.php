<?php
/**
 * Configure thumbnails and image related functions.
 *
 * @package Dax_Gutenberg_Starter
 */

 if ( ! function_exists( 'cruz_lorena_thumbnails' ) ) :

	/**
	 * Registra los diferentes tamaños de thumbnails.
	 */
	function las_mercedes_thumbnails() {

		// Imágenes optimizadas para posición horizontal.
		add_image_size( 'mobile', 480, 768, true );

		/**
		 * Registra los tamaños de thumbnails en el backend de WordPress.
		 *
		 * @param  array $sizes tamaño.
		 */
		function las_mercedes_custom_sizes( $sizes ) {
			return array_merge(
				$sizes, array(
					'mobile' => ( 'Mobile' ),
				)
			);
		}
		add_filter( 'image_size_names_choose', 'las_mercedes_custom_sizes' );

	} // End thumbnails.

	add_action( 'after_setup_theme', 'las_mercedes_thumbnails' );

 endif; // End if thumbnails function exists.

// Aumenta la calidad de los thumbnails.
add_filter( 'jpeg_quality', 'tgm_image_full_quality' );
add_filter( 'wp_editor_set_quality', 'tgm_image_full_quality' );
/**
 * Filters the image quality for thumbnails to be at the highest ratio possible.
 *
 * Supports the new 'wp_editor_set_quality' filter added in WP 3.5.
 *
 * @since 1.0.0
 *
 * @param int $quality  The default quality (90).
 * @return int $quality Amended quality (100).
 */
function tgm_image_full_quality( $quality ) {
	return 100;
}

/**
 * Cambia el icono del dashboard.
 */
function wpb_custom_logo() { ?>
<style type="text/css">
.wp-admin #wpadminbar #wp-admin-bar-site-name > .ab-item::before{
background-image: url("<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/favicon-32x32.png") !important;
background-position: center center;
background-size: contain;
color:rgba(0, 0, 0, 0);
background-repeat: no-repeat;
width: 32px;
height: 20px;
}
#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
background-position: 0 0;
}
</style>
<?php
}
add_action( 'wp_before_admin_bar_render', 'wpb_custom_logo' );
