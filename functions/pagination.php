<?php
/**
 * Función de paginación.
 *
 * @package Dax_Gutenberg_Starter
 */

if ( ! function_exists( 'dax_pagination' ) ) :

	/**
	 * Función que crea paginación sin necesidad de plugins.
	 */
	function dax_pagination() {

		global $new_query;
		global $wp_query;
		if ( $new_query ) {
			$this_query = $new_query;
		} else {
			$this_query = $wp_query;
		}
		$big            = 999999999;
		$paginate_links = paginate_links(
			array(
				'base'      => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
				'current'   => max( 1, get_query_var( 'paged' ) ),
				'total'     => $this_query->max_num_pages,
				'mid_size'  => 5,
				'prev_next' => true,
				'prev_text' => 'Anterior',
				'next_text' => 'Siguiente',
				'type'      => 'list',
			)
		);

		$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links );
		$paginate_links = str_replace( '<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links );
		$paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='current'><a href='#'>", $paginate_links );
		$paginate_links = str_replace( '</span>', '</a>', $paginate_links );
		$paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
		$paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );

		// Imprime la paginación si se encuentran más páginas.
		if ( $paginate_links ) {
			$tags = tags_permitidos();
			echo wp_kses( $paginate_links, $tags );
		}
	}
endif;
