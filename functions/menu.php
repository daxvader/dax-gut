<?php
/**
 * Registro de menús de navegación.
 *
 * @package Dax_Gutenberg_Starter
 */

if ( ! function_exists( 'dax_gut_menus' ) ) :

	/**
	 * Función que registra las posiciones de menús.
	 * Si se desea agregar más, pueden copiarse del arreglo
	 * y modificar su nombre.
	 */
	function dax_gut_menus() {
		$locations = array(
			'main'   => 'Main menu',
			'footer' => 'Footer menu',
		);
		register_nav_menus( $locations );
	}
	add_action( 'init', 'dax_gut_menus' );
endif;
