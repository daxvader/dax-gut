<?php
/**
 * Dax Gutenberg Starter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Dax_Gutenberg_Starter
 */

require get_template_directory() . '/functions/theme-support.php';

require get_template_directory() . '/functions/enqueue.php';

require get_template_directory() . '/functions/sidebars.php';

require get_template_directory() . '/functions/custom-header.php';

require get_template_directory() . '/functions/template-tags.php';

require get_template_directory() . '/functions/template-functions.php';

require get_template_directory() . '/functions/customizer.php';

if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/functions/jetpack.php';
}

if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/functions/woocommerce.php';
}
