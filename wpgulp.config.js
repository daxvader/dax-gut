// Gulp config based on https://github.com/ahmadawais/WPGulp

module.exports = {

	// Project options.
	projectURL: 'http://localhost/gut',
	productURL: './',
	browserAutoOpen: true,
	injectChanges: true,

	// Style options.
	styleSRC: './src/sass/styles.scss',
	styleDestination: './assets/css/',
	outputStyle: 'compact',
	errLogToConsole: true,
	precision: 10,

	// JS Vendor options.
	jsVendorSRC: './assets/js/vendor/*.js',
	jsVendorDestination: './assets/js/',
	jsVendorFile: 'vendors',

	// JS Custom options.
	jsCustomSRC: './src/js/custom/*.js',
	jsCustomDestination: './assets/js/',
	jsCustomFile: 'scripts',

	// Images options.
	imgSRC: './src/img/raw/**/*',
	imgDST: './assets/img/',

	// Watch files paths.
	watchStyles: './src/sass/**/*.scss',
	watchJsVendor: './src/js/vendor/*.js',
	watchJsCustom: './src/js/custom/*.js',
	watchPhp: './**/*.php',

	// Translation options.
	textDomain: 'WPGULP',
	translationFile: 'WPGULP.pot',
	translationDestination: './languages',
	packageName: 'WPGULP',
	bugReport: 'https://AhmadAwais.com/contact/',
	lastTranslator: 'Ahmad Awais <your_email@email.com>',
	team: 'AhmadAwais <your_email@email.com>',

	// Browsers you care about for autoprefixing. Browserlist https://github.com/ai/browserslist
	// The following list is set as per WordPress requirements. Though, Feel free to change.
	BROWSERS_LIST: [
		'last 2 version',
		'> 1%',
		'ie >= 11',
		'last 1 Android versions',
		'last 1 ChromeAndroid versions',
		'last 2 Chrome versions',
		'last 2 Firefox versions',
		'last 2 Safari versions',
		'last 2 iOS versions',
		'last 2 Edge versions',
		'last 2 Opera versions'
	]
};
